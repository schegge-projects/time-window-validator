package de.schegge.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ValidatorParameterResolver.class)
class LocalDateTimeWindowValidatorTest {

  private static class LocalDateTimeDto {

    @FutureWindow("P2D")
    LocalDateTime future;

    @FuturePresentWindow("P2D")
    LocalDateTime futurePresent;

    @PastWindow("P2D")
    LocalDateTime past;

    @PastPresentWindow("P2D")
    LocalDateTime pastPresent;
  }

  @Test
  void testCorrectLocalDateTimeFuture(Validator validator) {
    LocalDateTime future = LocalDateTime.now().plusDays(1L);
    LocalDateTimeDto dto = new LocalDateTimeDto();
    dto.future = future;
    dto.futurePresent = future;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectLocalDateTimePresent(Validator validator) {
    LocalDateTime now = LocalDateTime.now();
    LocalDateTimeDto dto = new LocalDateTimeDto();
    dto.pastPresent = now;
    dto.futurePresent = now;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectLocalDateTimePast(Validator validator) {
    LocalDateTime past = LocalDateTime.now().minusDays(1L);
    LocalDateTimeDto dto = new LocalDateTimeDto();
    dto.past = past;
    dto.pastPresent = past;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testIncorrectLocalDateTimeFuture(Validator validator) {
    LocalDateTimeDto dto = new LocalDateTimeDto();
    LocalDateTime past = LocalDateTime.now().minusDays(1L);
    dto.future = past;
    dto.futurePresent = past;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
    LocalDateTime future = LocalDateTime.now().plusDays(3L);
    dto.future = future;
    dto.futurePresent = future;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testIncorrectLocalDateTimePresent(Validator validator) {
    LocalDateTime now = LocalDateTime.now();
    LocalDateTimeDto dto = new LocalDateTimeDto();
    dto.past = now;
    dto.future = now;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testIncorrectLocalDateTimePast(Validator validator) {
    LocalDateTime now = LocalDateTime.now().plusDays(1L);
    LocalDateTimeDto dto = new LocalDateTimeDto();
    dto.past = now;
    dto.pastPresent = now;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
    LocalDateTime past = LocalDateTime.now().minusDays(3L);
    dto.past = past;
    dto.pastPresent = past;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testNull(Validator validator) {
    assertTrue(validator.validate(new LocalDateTimeDto()).isEmpty());
  }
}
