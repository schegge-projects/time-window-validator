package de.schegge.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.OffsetDateTime;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ValidatorParameterResolver.class)
class OffsetDateTimeWindowValidatorTest {

  private static class OffsetDateTimeDto {

    @FutureWindow("P2D")
    OffsetDateTime future;

    @FuturePresentWindow("P2D")
    OffsetDateTime futurePresent;

    @PastWindow("P2D")
    OffsetDateTime past;

    @PastPresentWindow("P2D")
    OffsetDateTime pastPresent;
  }

  @Test
  void testCorrectOffsetDateTimeFuture(Validator validator) {
    OffsetDateTime future = OffsetDateTime.now().plusDays(1L);
    System.out.println();
    OffsetDateTimeDto dto = new OffsetDateTimeDto();
    dto.future = future;
    dto.futurePresent = future;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectOffsetDateTimePresent(Validator validator) {
    OffsetDateTime now = OffsetDateTime.now();
    OffsetDateTimeDto dto = new OffsetDateTimeDto();
    dto.pastPresent = now;
    dto.futurePresent = now;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectLocalDatePast(Validator validator) {
    OffsetDateTime past = OffsetDateTime.now().minusDays(1L);
    OffsetDateTimeDto dto = new OffsetDateTimeDto();
    dto.past = past;
    dto.pastPresent = past;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testIncorrectOffsetDateTimeFuture(Validator validator) {
    OffsetDateTime past = OffsetDateTime.now().minusDays(1L);
    OffsetDateTimeDto dto = new OffsetDateTimeDto();
    dto.future = past;
    dto.futurePresent = past;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testIncorrectOffsetDateTimePresent(Validator validator) {
    OffsetDateTime now = OffsetDateTime.now();
    OffsetDateTimeDto dto = new OffsetDateTimeDto();
    dto.past = now;
    dto.future = now;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testIncorrectLocalDatePast(Validator validator) {
    OffsetDateTime now = OffsetDateTime.now().plusDays(1L);
    OffsetDateTimeDto dto = new OffsetDateTimeDto();
    dto.past = now;
    dto.pastPresent = now;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testNull(Validator validator) {
    assertTrue(validator.validate(new OffsetDateTimeDto()).isEmpty());
  }
}
