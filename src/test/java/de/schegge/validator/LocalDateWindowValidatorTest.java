package de.schegge.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ValidatorParameterResolver.class)
class LocalDateWindowValidatorTest {

  private static class LocalDateDto {

    @FutureWindow("P2D")
    LocalDate future;

    @FuturePresentWindow("P2D")
    LocalDate futurePresent;

    @PastWindow("P2D")
    LocalDate past;

    @PastPresentWindow("P2D")
    LocalDate pastPresent;
  }

  @Test
  void testCorrectLocalDateFuture(Validator validator) {
    LocalDate future = LocalDate.now().plusDays(1L);
    LocalDateDto dto = new LocalDateDto();
    dto.future = future;
    dto.futurePresent = future;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectLocalDatePresent(Validator validator) {
    LocalDate now = LocalDate.now();
    LocalDateDto dto = new LocalDateDto();
    dto.pastPresent = now;
    dto.futurePresent = now;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectLocalDatePast(Validator validator) {
    LocalDate past = LocalDate.now().minusDays(1L);
    LocalDateDto dto = new LocalDateDto();
    dto.past = past;
    dto.pastPresent = past;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testIncorrectLocalDateFuture(Validator validator) {
    LocalDate past = LocalDate.now().minusDays(1L);
    LocalDateDto dto = new LocalDateDto();
    dto.future = past;
    dto.futurePresent = past;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
    LocalDate future = LocalDate.now().plusDays(3L);
    dto.future = future;
    dto.futurePresent = future;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testIncorrectLocalDatePresent(Validator validator) {
    LocalDate now = LocalDate.now();
    LocalDateDto dto = new LocalDateDto();
    dto.past = now;
    dto.future = now;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testIncorrectLocalDatePast(Validator validator) {
    LocalDate now = LocalDate.now().plusDays(1L);
    LocalDateDto dto = new LocalDateDto();
    dto.past = now;
    dto.pastPresent = now;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
    LocalDate past = LocalDate.now().minusDays(3L);
    dto.past = past;
    dto.pastPresent = past;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testNull(Validator validator) {
    assertTrue(validator.validate(new LocalDateDto()).isEmpty());
  }
}
