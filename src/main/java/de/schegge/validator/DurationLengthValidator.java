package de.schegge.validator;

import static java.util.function.Predicate.not;

import java.time.Duration;
import java.util.Optional;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class DurationLengthValidator implements ConstraintValidator<DurationLength, Duration> {

  private Duration min;
  private Duration max;

  @Override
  public void initialize(DurationLength constraintAnnotation) {
    if (!constraintAnnotation.maxDuration().isEmpty() && constraintAnnotation.max() != 0) {
      throw new IllegalArgumentException("only maxDuration or max allowed");
    }
    if (!constraintAnnotation.minDuration().isEmpty() && constraintAnnotation.min() != 0) {
      throw new IllegalArgumentException("only minDuration or min allowed");
    }
    if (constraintAnnotation.max() < 0 || constraintAnnotation.min() < 0) {
      throw new IllegalArgumentException("only positive values allowed");
    }
    max = Optional.of(constraintAnnotation.maxDuration()).filter(not(String::isBlank)).map(Duration::parse)
        .orElseGet(() -> Duration.of(constraintAnnotation.max(), constraintAnnotation.unit()));
    min = Optional.of(constraintAnnotation.minDuration()).filter(not(String::isBlank)).map(Duration::parse)
        .orElseGet(() -> Duration.of(constraintAnnotation.min(), constraintAnnotation.unit()));
    if (!min.isZero() && !max.isZero() && min.compareTo(max) >= 0) {
      throw new IllegalArgumentException("min duration must be smaller than max duration: min=" + min + " max=" + max);
    }
  }

  @Override
  public boolean isValid(Duration value, ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    return (min.isZero() || min.compareTo(value) <= 0) && (max.isZero() || max.compareTo(value) >= 0);
  }
}
