package de.schegge.validator.window;

import de.schegge.validator.DateWindow;
import java.time.Period;
import java.time.temporal.Temporal;
import jakarta.validation.ConstraintValidator;

public abstract class AbstractDateWindowValidator<T extends Temporal> implements ConstraintValidator<DateWindow, T> {

  protected Period period;
  protected boolean past;
  protected boolean present;

  @Override
  public void initialize(DateWindow constraintAnnotation) {
    period = Period.parse(constraintAnnotation.value());
    if (period.isNegative()) {
      throw new IllegalArgumentException("period is negativ: " + period);
    }
    present = constraintAnnotation.present();
    past = constraintAnnotation.past();
  }
}
