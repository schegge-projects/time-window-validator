package de.schegge.validator.window;

import java.time.LocalDate;
import jakarta.validation.ConstraintValidatorContext;

public class LocalLocalDateWindowValidator extends AbstractLocalDateWindowValidator<LocalDate, LocalDate> {

  @Override
  public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
    return value == null || compareDates(value, LocalDate.now());
  }
}
