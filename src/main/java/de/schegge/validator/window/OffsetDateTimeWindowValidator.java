package de.schegge.validator.window;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.OffsetDateTime;
import jakarta.validation.ConstraintValidatorContext;

public class OffsetDateTimeWindowValidator extends AbstractDateWindowValidator<OffsetDateTime> {

  @Override
  public boolean isValid(OffsetDateTime value, ConstraintValidatorContext context) {
    return value == null || compareDates(value, OffsetDateTime.now(value.getOffset()).truncatedTo(DAYS));
  }

  protected boolean compareDates(OffsetDateTime value, OffsetDateTime now) {
    OffsetDateTime copy = value.truncatedTo(DAYS);
    if (present && now.isEqual(copy)) {
      return true;
    }
    if (past) {
      return now.isAfter(copy) && ((OffsetDateTime) period.subtractFrom(now)).isBefore(copy);
    }
    return now.isBefore(copy) && ((OffsetDateTime) period.addTo(now)).isAfter(copy);
  }
}
