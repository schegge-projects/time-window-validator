package de.schegge.validator.window;

import java.time.LocalDate;
import java.time.LocalDateTime;
import jakarta.validation.ConstraintValidatorContext;

public class LocalLocalDateTimeWindowValidator extends AbstractLocalDateWindowValidator<LocalDateTime, LocalDate> {

  @Override
  public boolean isValid(LocalDateTime value, ConstraintValidatorContext context) {
    return value == null || compareDates(value.toLocalDate(), LocalDateTime.now().toLocalDate());
  }
}
