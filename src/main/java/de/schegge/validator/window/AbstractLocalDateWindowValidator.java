package de.schegge.validator.window;

import java.time.chrono.ChronoLocalDate;
import java.time.temporal.Temporal;

public abstract class AbstractLocalDateWindowValidator<T extends Temporal, U extends ChronoLocalDate> extends
    AbstractDateWindowValidator<T> {

  protected boolean compareDates(U value, U now) {
    if (present && now.isEqual(value)) {
      return true;
    }
    if (past) {
      return now.isAfter(value) && ((ChronoLocalDate) period.subtractFrom(now)).isBefore(value);
    }
    return now.isBefore(value) && ((ChronoLocalDate) period.addTo(now)).isAfter(value);
  }
}
