package de.schegge.validator.window;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.ZonedDateTime;
import jakarta.validation.ConstraintValidatorContext;

public class ZonedDateTimeWindowValidator extends AbstractDateWindowValidator<ZonedDateTime> {

  @Override
  public boolean isValid(ZonedDateTime value, ConstraintValidatorContext context) {
    return value == null || compareDates(value, ZonedDateTime.now(value.getZone()).truncatedTo(DAYS));
  }

  protected boolean compareDates(ZonedDateTime value, ZonedDateTime now) {
    ZonedDateTime copy = ZonedDateTime.of(value.toLocalDateTime().truncatedTo(DAYS), value.getOffset());
    if (present && now.isEqual(copy)) {
      return true;
    }
    if (past) {
      return now.isAfter(copy) && ((ZonedDateTime) period.subtractFrom(now)).isBefore(copy);
    }
    return now.isBefore(copy) && ((ZonedDateTime) period.addTo(now)).isAfter(copy);
  }

}
